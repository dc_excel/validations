VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "InputValidation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private WithEvents oTextboxObject   As MSForms.TextBox
Attribute oTextboxObject.VB_VarHelpID = -1
Private WithEvents oComboboxObject  As MSForms.ComboBox
Attribute oComboboxObject.VB_VarHelpID = -1
Private WithEvents oCheckboxObject  As MSForms.CheckBox
Attribute oCheckboxObject.VB_VarHelpID = -1

Private oObject                     As Object
Private oForm                       As UserForm

Private bSubmitted                  As Boolean
Private bValid                      As Boolean

Private sExpression1                As String
Private sExpression2                As String
Private sExpr1                      As String
Private sExpr2                      As String
Private bRegExpIgnoreCase           As Boolean

Private sConditionExpression        As String
Private sConExpr                    As String

Private sErrorMessage               As String
Private sDefaultErrorMessage        As String

Private pValidationType             As InputValidationType
Private pDataType                   As xlDataType
Private pConditionType              As xlConditionType
Private pStopType                   As InputValidationStopType
Private pPerformSoftValidation      As Boolean
Private pChangeBackgroundColor      As Boolean
Private pChangeToolTip              As Boolean

'Errors will reformat the object
'Need to keep these properties to revert to them
Private sBackColor                  As String
Private sToolTip                    As String


Public Enum InputValidationType
    InputValidationIsNull = 1
    InputValidationIsNotNull
    InputValidationIsDate
    InputValidationNumeric
    InputValidationCharsInExpr1
    InputValidationContainedInExpr1
    InputValidationMatchesExpr1
    InputValidationNotExpr1
    InputValidationInRange
    InputValidationIsSheetName
    InputValidationGreaterThan
    InputValidationGreaterThanOrEqualTo
    InputValidationLessThan
    InputValidationLessThanOrEqualTo
    InputValidationBetween
    InputValidationLengthBetween
    InputValidationLengthGreaterThan
    InputValidationLengthLessThan
    InputValidationBooleanFuncInExpr1
    InputValidationRegExp
End Enum

Public Enum InputValidationStopType
    xlStopTypeHardStop = 1          'Won't let you continue without changing
    xlStopTypeSoftStop              'Warning, but if resubmitted will allow you to continue
End Enum

Public Enum xlConditionType
    xlConditionObjInConExprIsNotNull = 1
    xlConditionObjInConExprIsNull
    xlConditionBooleanFuncInExpr
End Enum

Public Enum xlDataType
    xlDataTypeNumeric = 1
    xlDataTypeString
    xlDataTypeDate
End Enum
'
'
'
'
'
'
'

Public Property Let Object(ByRef oNewObj As Object)

    Set oObject = oNewObj
    Set oForm = oNewObj.Parent
    
    
    'Set Property Defaults
    sBackColor = oNewObj.BackColor
    sToolTip = oNewObj.ControlTipText
    pStopType = xlStopTypeHardStop
    pPerformSoftValidation = True
    pChangeBackgroundColor = True
    pChangeToolTip = True
    
    'can't have withevents on a generic object
    'but need withevents for the soft validation
    Select Case TypeName(oNewObj)
        Case "TextBox"
            Set oTextboxObject = oNewObj
        Case "ComboBox"
            Set oComboboxObject = oNewObj
        Case "CheckBox"
            Set oCheckboxObject = oNewObj
    End Select

End Property

Public Property Get Object() As Object

    Set Object = oObject

End Property

Public Property Get Expression1() As String

    Expression1 = sExpression1
    
End Property

Public Property Let Expression1(ByVal sNew As String)

    sExpression1 = sNew
    
End Property

Public Property Get Expression2() As String

    Expression2 = sExpression2
    
End Property

Public Property Let Expression2(ByVal sNew As String)

    sExpression2 = sNew
    
End Property

Public Property Get ConditionExpression() As String

    ConditionExpression1 = sConditionExpression
    
End Property

Public Property Let ConditionExpression(ByVal sNew As String)

    sConditionExpression = sNew
    
End Property

Public Property Get ValidationType() As InputValidationType

    ValidationType = pValidationType
    
End Property

Public Property Let ValidationType(ByVal sNew As InputValidationType)

    pValidationType = sNew
    
End Property

Public Property Get ConditionType() As xlConditionType

    ConditionType = pConditionType

End Property

Public Property Let ConditionType(ByVal sNew As xlConditionType)

    pConditionType = sNew

End Property

Public Property Get StopType() As InputValidationStopType

    StopType = pStopType

End Property

Public Property Let StopType(sNew As InputValidationStopType)

    pStopType = sNew

End Property

Public Property Get DataType() As xlDataType

    DataType = pDataType
    
End Property

Public Property Let DataType(ByVal sNew As xlDataType)

    pDataType = sNew
    
End Property

Public Property Get ErrorMessage() As String

    ErrorMessage = sErrorMessage

End Property

Public Property Let ErrorMessage(ByVal sNew As String)

    sErrorMessage = sNew

End Property

Public Property Get RegExpIgnoreCase() As Boolean

    RegExpIgnoreCase = bRegExpIgnoreCase

End Property

Public Property Let RegExpIgnoreCase(ByVal bNew As Boolean)

    bRegExpIgnoreCase = bNew

End Property

Public Property Let PerformSoftValidation(ByVal bNew As Boolean)
    pPerformSoftValidation = bNew
End Property
Public Property Get PerformSoftValidation() As Boolean
    PerformSoftValidation = pPerformSoftValidation
End Property

Public Property Let ChangeBackgroundColor(ByVal bNew As Boolean)
    pChangeBackgroundColor = bNew
End Property
Public Property Get ChangeBackgroundColor() As Boolean
    ChangeBackgroundColor = pChangeBackgroundColor
End Property

Public Property Let ChangeToolTip(ByVal bNew As Boolean)
    pChangeToolTip = bNew
End Property
Public Property Get ChangeToolTip() As Boolean
    ChangeToolTip = pChangeToolTip
End Property

'
'
'
'
'


Private Sub oTextboxObject_Change()

    If pPerformSoftValidation Then
        Dim s As String
        Reset
        Validate s, False 'soft validate
    End If
    
    bSubmitted = False
        
End Sub

Private Sub oComboboxObject_Change()

    If pPerformSoftValidation Then
        Dim s As String
        Reset
        Validate s, False 'soft validate
    End If
    
    bSubmitted = False
    
End Sub

Private Sub oCheckboxObject_Change()
    
    If pPerformSoftValidation Then
        Dim s As String
        Reset
        Validate s, False 'soft validate
    End If
    
    bSubmitted = False
        
End Sub



Public Sub Reset()

    oObject.BackColor = sBackColor
    oObject.ControlTipText = sToolTip

End Sub


Public Function Validation_Length(TestObject As Object, _
                                Optional LengthMin As Variant = -1, _
                                Optional LengthMax As Variant = -1, _
                                Optional IsInclusive As Boolean = False, _
                                Optional DefaultErrorMessage As String) As Boolean
    
    Dim TestValueLength As Double
    Dim TestValue As Variant
    Dim TestObjectName As String
    
    On Error GoTo errhand
    
    With TestObject
        TestValue = .Value
        TestObjectName = .Name
    End With
    
    'short circut if null
    If TestValue = vbNullString Then
        Validation_Length = True
        Exit Function
    End If
    
    'short circut if no min or max
    If LengthMin = -1 And LengthMax = -1 Then
        Validation_Length = True
        Exit Function
    End If
    
    'get the length of the input string
    TestValueLength = Len(TestValue)
    
    
    If LengthMin = -1 Then 'max only - less than
        
        If IsInclusive Then
            Validation_Length = TestValueLength <= LengthMax
            DefaultErrorMessage = "Length of " & TestObjectName & " is not less than or equal to " & LengthMax & " characters"
        Else
            Validation_Length = TestValueLength < LengthMax
            DefaultErrorMessage = "Length of " & TestObjectName & " is not less than " & LengthMax & " characters"
        End If
        
    ElseIf LengthMax = -1 Then 'min only - greater than
        
        If IsInclusive Then
            Validation_Length = TestValueLength >= LengthMin
            DefaultErrorMessage = "Length of " & TestObjectName & " is not greater than or equal to " & LengthMin & " characters"
        Else
            Validation_Length = TestValueLength > LengthMin
            DefaultErrorMessage = "Length of " & TestObjectName & " is not greater than " & LengthMin & " characters"
        End If
        
    Else 'min and max - between
    
        If IsInclusive Then
            Validation_Length = TestValueLength >= LengthMin And _
                                    TestValueLength <= LengthMax
            DefaultErrorMessage = "Length of " & TestObjectName & " is not between " & LengthMin & " and " & LengthMax & " characters"
        Else
            Validation_Length = TestValueLength > LengthMin And _
                                    TestValueLength < LengthMax
            DefaultErrorMessage = "Length of " & TestObjectName & " is not between " & LengthMin - 1 & " and " & LengthMax - 1 & " characters"
        End If
        
    End If
    
    Exit Function
    
errhand:
    If Err.Number > 0 Then
        Validation_Length = False
        
        If Err.Number = 13 Then 'mismatch
            DefaultErrorMessage = TestObjectName & " is not a number"
        Else
            DefaultErrorMessage = Err.Number
        End If
        
        Err.Clear
    End If

End Function

Public Function Validation_Value(TestObject As Object, _
                                Optional ValueMin As Variant = -1, _
                                Optional ValueMax As Variant = -1, _
                                Optional IsInclusive As Boolean = False, _
                                Optional DefaultErrorMessage As String) As Boolean
    
    Dim TestValueNumber As Long
    Dim TestValue As Variant
    Dim TestObjectName As String
    
    On Error GoTo errhand
    
    TestValue = TestObject.Value
    TestObjectName = TestObject.Name
    
    'short circut if null
    If TestValue = vbNullString Then
        Validation_Value = True
        Exit Function
    End If
    
    'short circut if no min or max
    If ValueMin = -1 And ValueMax = -1 Then
        Validation_Value = True
        Exit Function
    End If
    
    
    'get the length of the input string
    TestValueNumber = CLng(TestValue)
    
    
    If ValueMin = -1 Then 'max only - less than
        
        If IsInclusive Then
            Validation_Value = TestValueNumber <= ValueMax
            DefaultErrorMessage = TestObjectName & " is not less than or equal to " & ValueMax
        Else
            Validation_Value = TestValueNumber < ValueMax
            DefaultErrorMessage = TestObjectName & " is not less than " & ValueMax
        End If
        
    ElseIf ValueMax = -1 Then 'min only - greater than
        
        If IsInclusive Then
            Validation_Value = TestValueNumber >= ValueMin
            DefaultErrorMessage = TestObjectName & " is not greater than or equal to " & ValueMin
        Else
            Validation_Value = TestValueNumber > ValueMin
            DefaultErrorMessage = TestObjectName & " is not greater than " & ValueMin
        End If
        
    Else 'min and max - between
    
        If IsInclusive Then
            Validation_Value = TestValueNumber >= ValueMin And _
                                    TestValueNumber <= ValueMax
            DefaultErrorMessage = TestObjectName & " is not between " & ValueMin & " and " & ValueMax
        Else
            Validation_Value = TestValueNumber > ValueMin And _
                                    TestValueNumber < ValueMax
            DefaultErrorMessage = TestObjectName & " is not between " & ValueMin - 1 & " and " & ValueMax - 1
        End If
        
    End If
    
    Exit Function

errhand:
    If Err.Number > 0 Then
        Validation_Value = False
        
        If Err.Number = 13 Then 'mismatch
            DefaultErrorMessage = TestObjectName & " is not a number"
        Else
            DefaultErrorMessage = Err.Number
        End If
        
        Err.Clear
    End If
    
End Function




Public Function Validate(ByRef sStatus As String, Optional ByVal pHardValidate As Boolean = True) As Boolean


On Error GoTo errhand

'***************************************************************
'****EARLY EXITS THAT DO NOT REQUIRE ADDITIONAL VALIDATION******
'***************************************************************
'
'
''''''Already submitted and valid without being changed
    If bSubmitted And bValid Then
        Validate = True
        Exit Function
    End If
    
''''''Soft Stop Already Submitted Once and Unchanged
    If bSubmitted And pStopType = xlStopTypeSoftStop Then
        Validate = True
        DisplayValidationResult Validate, sStatus
        Exit Function
    End If
    
'***************************************************************
'***************************************************************
'***************************************************************



'''''Set Submitted = True when hard validating. This would popup the message box
    bSubmitted = pHardValidate
   
   
   
'***************************************************************
'****************WHEN NO VALIDATION NEEDED**********************
'***************************************************************
'
    'does not meet the condition on the validation
    If CheckCondition = False Then
        Validate = True
        DisplayValidationResult Validate, sStatus
        Exit Function
    End If
    
    'if the value is null and we aren't checking if it's null
    If oObject.Value = vbNullString _
        And pValidationType <> InputValidationIsNotNull Then
        
        Validate = True
        DisplayValidationResult Validate, sStatus
        Exit Function
    End If

'***************************************************************
'***************************************************************
'***************************************************************



'Convert Expression without effecting original expression
If sExpression1 <> vbNullString Then
    sExpr1 = ReplaceObjects(sExpression1)
End If
If sExpression2 <> vbNullString Then
    sExpr2 = ReplaceObjects(sExpression2)
End If


   
'***************************************************************
'************************VALIDATE*******************************
'***************************************************************
'
    Select Case pValidationType
    
        Case InputValidationBetween

            Validate = Validation_Value(TestObject:=oObject, ValueMin:=sExpr1, _
                                            ValueMax:=sExpr2, IsInclusive:=True)
            
        Case InputValidationGreaterThan
        
            Validate = Validation_Value(TestObject:=oObject, ValueMin:=sExpr1, IsInclusive:=False, _
                                            DefaultErrorMessage:=sDefaultErrorMessage)
        
        Case InputValidationGreaterThanOrEqualTo
        
            Validate = Validation_Value(TestObject:=oObject, ValueMin:=sExpr1, IsInclusive:=True, _
                                            DefaultErrorMessage:=sDefaultErrorMessage)
        
        Case InputValidationLessThan
        
            Validate = Validation_Value(TestObject:=oObject, ValueMax:=sExpr1, IsInclusive:=False, _
                                            DefaultErrorMessage:=sDefaultErrorMessage)
        
        Case InputValidationLessThanOrEqualTo
        
            Validate = Validation_Value(TestObject:=oObject, ValueMax:=sExpr1, IsInclusive:=True, _
                                            DefaultErrorMessage:=sDefaultErrorMessage)
            
        Case InputValidationIsNotNull
            
            Validate = Validation_IsNotNull(oObject)
        
        Case InputValidationIsNull
            
            Validate = Validation_IsNull(oObject)
            
        Case InputValidationNumeric
            
            Validate = Validation_Numeric(oObject)
            
        Case InputValidationCharsInExpr1
            
            Validate = Validation_CharsInExpr1(oObject)
        
        Case InputValidationContainedInExpr1
            
            Validate = Validation_ContainedInExpr1(oObject)
        
        Case InputValidationIsDate
            
            Validate = Validation_IsDate(oObject)
        
        Case InputValidationMatchesExpr1
            
            Validate = Validation_MatchesExpr1(oObject)
        
        Case InputValidationNotExpr1
            
            Validate = Validation_NotExpr1(oObject)
        
        Case InputValidationInRange
            
            Validate = Validation_InRange(oObject)
    
        Case InputValidationIsSheetName
        
            Validate = Validation_IsSheetName(oObject)
        
        Case InputValidationLengthBetween
            
            Validate = Validation_Length(TestObject:=oObject, LengthMin:=sExpr1, _
                                                LengthMax:=sExpr2, IsInclusive:=True, _
                                                DefaultErrorMessage:=sDefaultErrorMessage)
        
        Case InputValidationLengthGreaterThan
        
            Validate = Validation_Length(TestObject:=oObject, LengthMin:=sExpr1, IsInclusive:=False, _
                                            DefaultErrorMessage:=sDefaultErrorMessage)
            
        Case InputValidationLengthLessThan
        
            Validate = Validation_Length(TestObject:=oObject, LengthMax:=sExpr1, IsInclusive:=False, _
                                            DefaultErrorMessage:=sDefaultErrorMessage)
        
        Case InputValidationBooleanFuncInExpr1
        
            Validate = Validation_BooleanFuncInExpr1
        
        Case InputValidationRegExp
            
            Validate = Validation_RegExp(oObject)
        
    End Select                                      'Select Case pValidationType


    DisplayValidationResult Validate, sStatus


Exit Function

'***************************************************************
'***************************************************************
'***************************************************************


   
'***************************************************************
'******************ERROR HANDLING*******************************
'***************************************************************
errhand:
DisplayValidationResult False, sStatus

End Function

Private Sub DisplayValidationResult(bValidated As Boolean, ByRef sStatus As String)
    
    Dim FinalErrorMessage As String
    
    bValid = bValidated

    If bValidated Then
    
        sStatus = vbNullString
        
        If InStr(oObject.Tag, "|ERROR:") = 0 Then
            oObject.BackColor = sBackColor
            oObject.ControlTipText = sToolTip
        End If
            
    Else
    
        If pChangeBackgroundColor Then
            Select Case pStopType
                Case xlStopTypeHardStop
                    oObject.BackColor = vbRed
                Case xlStopTypeSoftStop
                    oObject.BackColor = vbYellow
            End Select
        End If
        
        If sErrorMessage = vbNullString Then
            FinalErrorMessage = sDefaultErrorMessage
        Else
            FinalErrorMessage = sErrorMessage
        End If
        
        sStatus = FinalErrorMessage
        oObject.Tag = oObject.Tag & "|ERROR: " & FinalErrorMessage
        
        If pChangeToolTip Then
            oObject.ControlTipText = oObject.ControlTipText & "|ERROR: " & FinalErrorMessage
        End If
        
        
        
    End If

End Sub

Private Function CheckCondition() As Boolean

'If no condition then return true
If sConditionExpression = vbNullString Then
    CheckCondition = True
    Exit Function
End If

sConExpr = ReplaceObjects(sConditionExpression)


Select Case pConditionType

    Case xlConditionObjInConExprIsNotNull
        
        CheckCondition = CheckCondition_ObjInConExprIsNotNull

    Case xlConditionObjInConExprIsNull
        
        CheckCondition = CheckCondition_ObjInConExprIsNull
        
    Case xlConditionBooleanFuncInExpr
        
        CheckCondition = CheckCondition_BooleanFuncInExpr
        
End Select




End Function

Private Function ReplaceObjects(ByVal sExpress As String) As String

Dim sObjectName As String
Dim sReplaceStr As String
Dim iStart As Long


Do Until InStr(sExpress, "{") = 0

    iStart = InStr(sExpress, "{")
    sReplaceStr = Mid$(sExpress, iStart, (InStr(sExpress, "}") + 1) - iStart)
    sObjectName = Mid$(sReplaceStr, 2, Len(sReplaceStr) - 2)
    
    sExpress = Replace(sExpress, sReplaceStr, oForm.Controls(sObjectName).Value)

Loop

ReplaceObjects = sExpress
    
    
End Function
Private Function Validation_IsNull(pObject As Object) As Boolean

    sDefaultErrorMessage = pObject.Name & " must be null"

    If pObject.Value = vbNullString Or (TypeName(pObject) = "CheckBox" And pObject.Value = True) Then
        Validation_IsNull = True
    Else
        Validation_IsNull = False
    End If

End Function

Private Function Validation_IsNotNull(pObject As Object) As Boolean
    
    sDefaultErrorMessage = pObject.Name & " is required"

    If pObject.Value = vbNullString Or (TypeName(pObject) = "CheckBox" And pObject.Value = False) Then
        Validation_IsNotNull = False
    Else
        Validation_IsNotNull = True
    End If

End Function

Private Function Validation_Numeric(pObject As Object) As Boolean

    sDefaultErrorMessage = pObject.Name & " must be numeric"

    Validation_Numeric = IsNumeric(pObject.Value)

End Function



Private Function Validation_CharsInExpr1(pObject As Object) As Boolean

Dim lThisChar As Long
Dim sThisChar As String
Dim pValue As String
    
    sDefaultErrorMessage = pObject.Name & " must only contain " & sExpr1
    
    pValue = pObject.Value

    For lThisChar = 1 To Len(pValue)
        sThisChar = Mid$(pValue, lThisChar, 1)
        If InStr(sExpr1, sThisChar) = 0 Then
            Validation_CharsInExpr1 = False
            Exit Function
        End If
    Next
    
    Validation_CharsInExpr1 = True


End Function

Private Function Validation_ContainedInExpr1(pObject As Object) As Boolean

    sDefaultErrorMessage = pObject.Name & " must be in " & sExpr1
    
    Validation_ContainedInExpr1 = InStr(sExpr1, pObject.Value) > 0

End Function

Private Function Validation_MatchesExpr1(pObject As Object) As Boolean
    
    sDefaultErrorMessage = pObject.Name & " must match " & sExpr1
    
    Validation_MatchesExpr1 = (sExpr1 = pObject.Value)

End Function

Private Function Validation_NotExpr1(pObject As Object) As Boolean

    sDefaultErrorMessage = pObject.Name & " must not match " & sExpr1

    Validation_NotExpr1 = (sExpr1 <> pObject.Value)

End Function

Private Function Validation_IsDate(pObject As Object) As Boolean

    sDefaultErrorMessage = pObject.Name & " must be a date"

    If IsDate(pObject.Value) Then
        Validation_IsDate = True
    Else
        Validation_IsDate = False
    End If

End Function

Private Function Validation_InRange(pObject As Object) As Boolean
    
    sDefaultErrorMessage = pObject.Name & " must be found in the range " & sExpr1

    Validation_InRange = WorksheetFunction.CountIf(Range(sExpr1), pObject.Value) > 0

End Function

Private Function Validation_IsSheetName(pObject As Object) As Boolean

Dim k As Integer
Dim iCount As Integer
Dim pValue As String

    pValue = pObject.Value
    iCount = ActiveWorkbook.Sheets.Count
    sDefaultErrorMessage = pObject.Name & " must be a sheet name"
    
    For k = 1 To iCount
    
        If ActiveWorkbook.Sheets(k).Name = pValue Then
            Validation_IsSheetName = True
            Exit Function
        End If
        
    Next
    
    Validation_IsSheetName = False


End Function

Private Function Validation_BooleanFuncInExpr1() As Boolean

'this will call a public function in the project
'you can pass parameters by listing them after the name of the function separated by commas

'EXAMPLE
'Function SampleBooleanFunction(Params As Variant) As Boolean
'    If Params(0) = "T" Then
'        SampleBooleanFunction = True
'    Else
'        SampleBooleanFunction = False
'    End If
'End Function

Dim sArr() As String
Dim sFunctionName As String
Dim sRest As String

    sDefaultErrorMessage = oObject.Name & " does not pass boolean function check"
    
    If InStr(sExpr1, ",") Then
        sFunctionName = Left$(sExpr1, InStr(sExpr1, ",") - 1)
        sRest = Right$(sExpr1, Len(sExpr1) - Len(sFunctionName) - 1)
        sArr = Split(sRest, ",")
        Validation_BooleanFuncInExpr1 = Run(sFunctionName, sArr())
    Else
        Validation_BooleanFuncInExpr1 = Run(sExpr1)
    End If


End Function

Private Function Validation_RegExp(pObject As Object) As Boolean


    Dim RegEx As Object
    Set RegEx = CreateObject("VBScript.RegExp")
        
    sDefaultErrorMessage = pObject.Name & " does not match the pattern " & sExpr1
        
    RegEx.Pattern = sExpr1
    RegEx.IgnoreCase = bRegExpIgnoreCase
    
    Validation_RegExp = RegEx.test(pObject.Value)
        
    Set RegEx = Nothing


End Function

Private Function CheckCondition_ObjInConExprIsNotNull() As Boolean

    CheckCondition_ObjInConExprIsNotNull = sConExpr <> vbNullString

End Function

Private Function CheckCondition_ObjInConExprIsNull() As Boolean

    CheckCondition_ObjInConExprIsNull = sConExpr = vbNullString

End Function

Private Function CheckCondition_BooleanFuncInExpr() As Boolean

    Dim sArr() As String
    Dim sFunctionName As String
    Dim sRest As String

    If InStr(sConExpr, ",") Then
        sFunctionName = Left$(sConExpr, InStr(sConExpr, ",") - 1)
        sRest = Right$(sConExpr, Len(sConExpr) - Len(sFunctionName) - 1)
        sArr = Split(sRest, ",")
        CheckCondition_BooleanFuncInExpr = Run(sFunctionName, sArr())
    Else
        CheckCondition_BooleanFuncInExpr = Run(sConExpr)
    End If

End Function




