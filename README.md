![](img/errormsg.png)
# Userform Validations For Excel
Declaratively configure input validations for custom userforms in Excel using an array of prebuilt options

## Features
* Choose to make a validation a hard stop or an overridable warning
* Validate as users change the value in the field or on form submission
* Inline notifications through coloring and tooltips or message box popups
* Dynamic value substitutions allow you to use values in other inputs of the userform as part of the validation
* Conditionally perform validations
* Extendable through the use of the InputValidationBooleanFuncInExpr1 validation type

## Example Code

This repository contains and example Excel doc with a sample implementation on a Userform using a variety of the features and validation types.

									
## Dynamic Substitutions
Any validation utilizing expression1 or expression2 properties can take advantage of item value substition. Expression1 or expression2 can be set to the value of another item on the userform by setting the property to the name of the target item in braces {}

```
Set lVal = New InputValidation

With lVal
    .Object = Me.tbPassword
    .ValidationType = InputValidationMatchesExpr1
    .Expression1 = "{tbPassword2}"
    .ErrorMessage = "Passwords do not match"
End With
```
									
## Class: UserformValidation

### Object
Set or return the userform object to be validated

### Validations
Collection of InputValdations. Add new input validations here as you create them.

### ErrorType
`xlErrorTypeInlineOnly` Highlights errors but does not popup a message box

`xlErrorTypeMsgBox` Highlights errors and displays them in a message box


## Class: InputValidation
### Object
Pass the textbox, combobox, or checkbox you would like to perform validation on

### ValidationType
Type of validation to perform. Validation types are listed below

### ErrorMessage
The message to display when validation fails

### Expression1
A variant with varied uses depending on the validation type

### Expression2
A variant with varied uses depending on the validation type

### StopType
`xlStopTypeHardStop` will not allow the form to be submitted until the input is valid.

`xlStopTypeSoftStop` will display a warning that can be overridden by the user. 

Default is `xlStopTypeHardStop`

### ConditionType
If you want to conditionally perform the validation, you can add a condition type.

`xlConditionObjInConExprIsNotNull` will check if an object in ConditionExpression in {} notation has value, and if it does, the validation will be performed

`xlConditionObjInConExprIsNull` will check if an object in ConditionExpression in {} notation has no value, and if it does not, the validation will be performed

`xlConditionBooleanFuncInExpr` will check the results of a boolean function whose name is listed in ConditionExpression, and if it returns true, the validation will be performed.

### ConditionExpression
A variant with varied uses depending on ConditionType

### ChangeBackgroundColor
When set to true, the input control background will change color to indicate an error. Default is `true`.

### ChangeToolTip
When set to true, the input control tooltip will change to indicate an error. Default is `true`.

### PerformSoftValidation
When set to `true`, the input will be validated upon update. Background color and tooltip will be updated live without form submission. Default is `true`.

### RegExpIgnoreCase
When set to `true`, a regular expression validation will ignore the case of the text.

### Reset
Clears any formatting changed as a result of invalid input

### Validate
Check the validity of the input

## Enum InputValidationType
### InputValidationBetween
Value is between the value in expression1 and expression2

```
Dim l_Validation As InputValidation
Set l_Validation = New InputValidation
									
With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationBetween
	.Expression1 = 1
	.Expression2 = 10
	.ErrorMessage = "Value must be between 1 and 10"
End With
```

### InputValidationGreaterThan
Value is greater than the value in expression1

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationGreaterThan
	.Expression1 = 1
	.ErrorMessage = "Value must be greater than 1"
End With
```

### InputValidationGreaterThanOrEqualTo
Value is greater than or equal to the value in expression1

```
Dim l_Validation As InputValidation

Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationGreaterThanOrEqualTo
	.Expression1 = 1
	.ErrorMessage = "Value must be greater than or equal to 1"
End With
```

### InputValidationLessThanOrEqualTo
Value is less than or equal to the value in expression1

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationLessThanOrEqualTo
	.Expression1 = 10
	.ErrorMessage = "Value must be less than or equal to 10"
End With
```

### InputValidationLessThan
Value is less than the value in expression1

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationLessThan
	.Expression1 = 10
	.ErrorMessage = "Value must be less than 10"
End With
```

### InputValidationIsNull
Value is null

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationIsNull
	.ErrorMessage = "Value must be null"
End With
```

### InputValidationIsNotNull
Contains a value

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationIsNotNull
	.ErrorMessage = "Input must have a value"
End With
```

### InputValidationNumeric
Value is a number

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationNumeric
	.ErrorMessage = "Value must be numeric"
End With
```

### InputValidationIsDate
Value can be interpreted as a date

```
Dim l_Validation As InputValidation

Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationIsDate
	.ErrorMessage = "Value must be a date"
End With
```

### InputValidationCharsInExpr1
Value only contains characters contained in expression1

```
Dim l_Validation As InputValidation

Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationCharsInExpr1

	.Expression1 = "1234567890."
	.ErrorMessage = "Value contains invalid characters"
End With
```

### InputValidationContainedInExpr1
The entire value is contained within the value of expression 1

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationContainedInExpr1

	.Expression1 = "Bob,Joe,Jim"
	.ErrorMessage = "Name must be Bob, Joe, or Jim"
End With
```

### InputValidationMatchesExpr1
Value equals the value in expression 1

```
Dim l_Validation As InputValidation

Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Password
	.ValidationType = InputValidationMatchesExpr1

	.Expression1 = {PasswordConfirm}
	.ErrorMessage = "Password fields do not match"
End With
```

### InputValidationNotExpr1
Value does not equal the value in expression 2

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Password
	.ValidationType = InputValidationNotExpr1

	.Expression1 = {UserName}
	.ErrorMessage = "Password cannot be the same as username"
End With
```

### InputValidationInRange
Value is contained somewhere in the range provided in expression 1

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationInRange

	.Expression1 = "Sheet1!A:A"
	.ErrorMessage = "Value must appear in Column A of Sheet 1"
End With
```

### InputValidationIsSheetName
Value must be the name of a sheet in the active workbook

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationIsSheetName
	.ErrorMessage = "Value must be the name of a sheet"
End With
```

### InputValidationLengthBetween
Value's length must be between the value in expression 1 and the value in expression 2

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationLengthBetween

	.Expression1 = 7

	.Expression2 = 12
	.ErrorMessage = "Value length should be between 7 and 12 characters"
End With
```

### InputValidationLengthGreaterThan
Value's length must be greater than the number in expression 1

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationLengthGreaterThan

	.Expression1 = 5
	.ErrorMessage = "Value must be longer than 5 characters"
End With
```

### InputValidationLengthLessThan
Value's length must be less than the number in expression 1

```
Dim l_Validation As InputValidation

Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationLengthLessThan

	.Expression1 = 30
	.ErrorMessage = "Value must be less than 30 characters"
End With
```

### InputValidationBooleanFuncInExpr1
VBA function in expression 1 must return a boolean of true

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationBooleanFuncInExpr1

	.Expression1 = "MacroBool"
	.ErrorMessage = "Value does not meet specified criteria"
End With
```

### InputValidationRegExp
Value matches a regular expression in expression 1

```
Dim l_Validation As InputValidation
    
Set l_Validation = New InputValidation

With l_Validation
	.Object = Me.Textbox1
	.ValidationType = InputValidationRegExp

	 .Expression1 = "\d{3,10}"
	.ErrorMessage = "value is not a valid postal code"
End With
```		