VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "UserformValidation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private colValidations  As Collection
Private oForm           As UserForm
Private bValid          As Boolean
Private sErrorMessage   As String
Private pErrorType      As InputValidationErrorType


Public Enum InputValidationErrorType
    xlErrorTypeInlineOnly = 1
    xlErrorTypeMsgBox
End Enum

Private Sub Class_Initialize()
Set colValidations = New Collection
End Sub

Public Property Let Object(ByRef oNewObj As Object)

    Set oForm = oNewObj
    pErrorType = xlErrorTypeMsgBox
    sErrorMessage = "Error Submitting Form"

End Property

Public Property Get Object() As Object

    Set Object = oObject

End Property

Public Property Get Validations() As Collection

    Set Validations = colValidations
    
End Property

Public Property Set Validations(sNew As Collection)

    Set colValidations = sNew

End Property

Public Property Get ErrorType() As InputValidationErrorType

ErrorType = pErrorType

End Property

Public Property Let ErrorType(sNew As InputValidationErrorType)

pErrorType = sNew

End Property

Public Property Get ErrorMessage() As String

ErrorMessage = sErrorMessage

End Property

Public Property Let ErrorMessage(sNew As String)

sErrorMessage = sNew

End Property


Public Function Validate(Optional ByRef sHardStops As String = vbNullString, _
                            Optional ByRef sSoftStops As String = vbNullString, _
                            Optional ByRef sFullMessage As String = vbNullString) As Boolean

Dim sMsg            As String
Dim bHard           As Boolean
Dim bSoft           As Boolean
Dim vResp           As Variant

ResetAll


For k = 1 To colValidations.Count

    If colValidations(k).Validate(sMsg) = False Then
        
        Select Case colValidations(k).StopType
            Case xlStopTypeHardStop
                sHardStops = sHardStops & Chr(13) & Chr(149) & sMsg
                bValid = False
                bHard = True
            Case xlStopTypeSoftStop
                sSoftStops = sSoftStops & Chr(13) & Chr(149) & sMsg
                bValid = False
                bSoft = True
        End Select
        
    End If

Next


If bHard And bSoft Then
    sFullMessage = "Please correct the following errors: " & _
                        sHardStops
                        
        sFullMessage = sFullMessage & Chr(13) & Chr(13) & _
            "You should consider correcting the following warnings: " & _
                sSoftStops
ElseIf bHard Then
    sFullMessage = "Please correct the following errors: " & _
                        sHardStops
ElseIf bSoft Then
    sFullMessage = "You should consider correcting the following warnings: " & _
                     sSoftStops & Chr(13) & Chr(13) & _
                        "Would you like to submit anyway?"
End If



If Not bValid And bHard And pErrorType = xlErrorTypeMsgBox Then
    MsgBox sFullMessage, vbCritical, sErrorMessage
    Validate = False
ElseIf Not bValid And Not bHard And pErrorType = xlErrorTypeMsgBox Then
    vResp = MsgBox(sFullMessage, vbYesNo, sErrorMessage)
        If vResp = vbYes Then
            Validate = True
        Else
            Validate = False
        End If
ElseIf Not bValid Then
    Validate = False
Else
    Validate = True
End If


End Function

Private Sub ResetAll()

For k = 1 To colValidations.Count

    colValidations(k).Reset

Next

bValid = True


End Sub


